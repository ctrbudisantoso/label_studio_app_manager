# todo/todo_api/views.py
import os.path
import random

from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
# from rest_framework import permissions
from .models import Attribute, Project, ProjectSerializer
import requests
import os
import re
from yolorest.service import make_labels, replace_in_file
import uuid
import shutil
from label_studio_converter.imports.yolo import convert_yolo_to_ls


# from .forms import UploadFileForm

class ProjectCrudApiView(APIView):

    def get(self, request, *args, **kwargs):
        pr_id = kwargs.get("id")
        if pr_id == 0:
            data = Project.objects.all().values()
            return Response(data, status=status.HTTP_200_OK)

        data = Project.objects.filter(project_id=pr_id).values()
        el = {}
        if data.count() > 0:
            el = data[0]

        return Response(el, status=status.HTTP_200_OK)

    def post(self, request, *args, **kwargs):
        images = request.FILES.getlist('image')
        title = request.POST.get('title')
        description = request.POST.get('description')
        workspace_path = Attribute.get_label_studio_workspace_path()
        folder_name = re.sub("\s", "_", str(title))
        project_path = os.path.join(workspace_path, folder_name)
        temp_path = os.path.join(os.getcwd(), "temp")
        if not os.path.exists(temp_path):
            os.makedirs(temp_path)
        source_path = os.path.join(temp_path, uuid.uuid4().hex)
        # print(source_path)
        if not os.path.exists(source_path):
            os.makedirs(source_path)
        count = 0

        train_count = 0
        val_count = 0
        test_count = 0
        for image in images:
            image_name = str(image)
            count += 1
            extent = ""
            if re.search("\.[a-zA-Z]+$", image_name):
                extent = re.search("\.[a-zA-Z]+$", image_name)[0]
                print(extent)
            filename = folder_name + "_" + uuid.uuid4().hex + extent

            rand_number = random.randint(0, 99)
            if rand_number < 80:
                train_count += 1
                filename = 'train_' + str(train_count) + "_" + filename
            elif rand_number < 90:
                val_count += 1
                filename = 'val_' + str(val_count) + "_" + filename
            else:
                test_count += 1
                filename = 'test_' + str(test_count) + "_" + filename
            filename = os.path.join(source_path, str(filename))
            with open(filename, "wb+") as destination:
                for chunk in image.chunks():
                    destination.write(chunk)
        model_used = request.POST.get('model-used')
        result = self.create_project(title, description, project_path, source_path,model_used)
        shutil.rmtree(source_path)
        p = Project()
        p.project_id = result['id']
        p.title = result['title']
        p.description = result['description']
        p.active = True
        p.task_number = result['task_number']
        p.finished = False
        p.save()
        return Response(result, status=status.HTTP_202_ACCEPTED)

    def put(self, request, *args, **kwargs):
        return Response("{}", status=status.HTTP_202_ACCEPTED)

    def create_project(self, project_name, description, project_path, source_path,model_used):
        print('source_path')
        print(source_path)
        url = Attribute.get_label_studio_url()
        token = Attribute.get_label_studio_token()
        complete_url = url + '/api/projects'
        try:
            r = requests.post(complete_url, headers={"Authorization": "Token " + token}, data={
                'title': project_name,
                'description': description,
                'label_config': Attribute.get_label_studio_label_config()
            })

            json_res = r.json()
            result_id = json_res['id']
            project_path = make_labels(source_path, project_path,model_used)
            f = open(os.path.join(project_path, "classes.txt"), "w")
            f.write(Attribute.get_label_studio_classes())
            f.close()

            label_path = os.path.join(project_path, "labels")
            if model_used == 'yolov5n.pt':
                for file_name in os.listdir(label_path):
                    replace_in_file(os.path.join(label_path,file_name), "^1", "5")
                    replace_in_file(os.path.join(label_path,file_name), "^5", "0")

            image_path = os.path.join(project_path, "images")
            if not os.path.exists(image_path):
                os.makedirs(image_path)

            dir_title = re.search("\w+$", str(project_path))[0]
            print('dir_title')
            print(dir_title)
            for file_name in os.listdir(source_path):
                source = os.path.join(source_path, file_name)
                destination = os.path.join(image_path, file_name)
                print('destination')
                print(destination)
                if os.path.isfile(source):
                    shutil.copy(source, destination)
            add_dataset_url = url + '/api/storages/localfiles?project=' + str(result_id)
            r1 = requests.post(add_dataset_url, headers={"Authorization": "Token " + token}, data={
                "project": str(result_id),
                "title": "",
                "regex_filter": "",
                "path": image_path,
                "use_blob_urls": False
            })
            temp_conf_file = os.path.join(project_path, 'labels_content.json')
            convert_yolo_to_ls(project_path, temp_conf_file)

            print('dir_title')
            print(dir_title)
            subst = dir_title+ "/images"
            print('subst')
            print(subst)

            # f = open(temp_conf_file, "r")
            # cnt = f.read()
            # f.close()

            replace_in_file(temp_conf_file, "?d=/", "?d=" + dir_title+"/images/")

            post_import_url = url + '/api/projects/' + str(result_id) + "/import?commit_to_project=true"
            multipart_form_data = {
                'upload': ('labels_content.json', open(temp_conf_file, 'rb'))
            }
            requests.post(post_import_url, headers={"Authorization": "Token " + token}, files=multipart_form_data)

            get_url = url + '/api/projects/' + str(result_id)
            r2 = requests.get(get_url, headers={"Authorization": "Token " + token})
            return r2.json()
        except:
            return 0


class ProjectSyncApiView(APIView):
    def get(self, request, *args, **kwargs):
        url = Attribute.get_label_studio_url()
        token = Attribute.get_label_studio_token()
        from_server = []
        complete_url = url + '/api/projects?page=1&page_size=30&include=id,description,title,task_number'
        r = requests.get(complete_url, headers={"Authorization": "Token " + token})
        results = r.json()['results']
        next_page = r.json()['next']
        for result in results:
            proj = {
                'project_id': result['id'],
                'title': result['title'],
                'description': result['description'],
                'task_number': result['task_number']

            }
            from_server.insert(0, proj)

        while next_page is not None:
            r = requests.get(next_page, headers={"Authorization": "Token " + token})
            next_results = r.json()['results']
            next_page = r.json()['next']
            for result in next_results:
                proj = {
                    'project_id': result['id'],
                    'title': result['title'],
                    'description': result['description'],
                    'task_number': result['task_number']

                }
                from_server.insert(0, proj)

        projects = Project.objects.order_by('project_id').all()
        serializer = ProjectSerializer(projects, many=True)
        xml = {'server': from_server, 'db': serializer.data}
        return Response(xml, status=status.HTTP_200_OK)
