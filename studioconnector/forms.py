from django import forms


class UploadFileForm(forms.Form):
    # file = forms.FileField()
    image = forms.FileField(widget=forms.ClearableFileInput(attrs={'multiple': True}))


class SyncProjectForm(forms.Form):
    project_id = forms.CharField(widget=forms.HiddenInput())


class UploadYoloModelForm(forms.Form):
    model_name = forms.CharField(label="Nama model", max_length=255)
    model_file = forms.FileField(label="File model (.pt)")
