from django.db import models

# Create your models here.
from django.db import models
from rest_framework import serializers


class Post(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField()
    image = models.FileField(blank=True)

    def __str__(self):
        return self.title

class YoloModel(models.Model):
    model_name = models.CharField(max_length=255)
    model_path = models.CharField(max_length=255)
    used_as_default = models.BooleanField(default=False)


class Attribute(models.Model):
    key = models.CharField(max_length=255)
    value = models.TextField()

    __label_studio_url = ""
    __label_studio_token = ""
    __label_studio_workspace_path = ""
    __label_studio_label_config = ""
    __label_studio_classes = ""

    @classmethod
    def reload_attributes(cls):
        attributes = Attribute.objects.all().values()

        for attribute in attributes:
            if attribute['key'] == 'label_studio_url':
                cls.__label_studio_url = attribute['value']
            if attribute['key'] == 'label_studio_token':
                cls.__label_studio_token = attribute['value']
            if attribute['key'] == 'label_studio_workspace_path':
                cls.__label_studio_workspace_path = attribute['value']
            if attribute['key'] == 'label_studio_label_config':
                cls.__label_studio_label_config = attribute['value']
            if attribute['key'] == 'label_studio_classes':
                cls.__label_studio_classes = attribute['value']

    @classmethod
    def get_label_studio_url(cls):
        if cls.__label_studio_url == "":
            cls.reload_attributes()
        return cls.__label_studio_url

    @classmethod
    def get_label_studio_token(cls):
        if cls.__label_studio_token == "":
            cls.reload_attributes()
        return cls.__label_studio_token

    @classmethod
    def get_label_studio_workspace_path(cls):
        if cls.__label_studio_workspace_path == "":
            cls.reload_attributes()
        return cls.__label_studio_workspace_path

    @classmethod
    def get_label_studio_label_config(cls):
        if cls.__label_studio_label_config == "":
            cls.reload_attributes()
        return cls.__label_studio_label_config

    @classmethod
    def get_label_studio_classes(cls):
        if cls.__label_studio_classes == "":
            cls.reload_attributes()
        return cls.__label_studio_classes


class Project(models.Model):
    project_id = models.IntegerField()
    title = models.CharField(max_length=255)
    description = models.CharField(max_length=1024)
    task_number = models.IntegerField()
    finished = models.BooleanField(default=False)
    active = models.BooleanField(default=False)

    def create(self, validated_data):
        return Project.objects.create(**validated_data)

    def update(self, instance, validated_data):
        instance.project_id = validated_data.get('title', instance.project_id)
        instance.title = validated_data.get('title', instance.title)
        instance.description = validated_data.get('description', instance.description)
        instance.task_number = validated_data.get('title', instance.task_number)
        instance.finished = validated_data.get('title', instance.finished)
        instance.active = validated_data.get('title', instance.active)

        instance.save()
        return instance
    # def __str__(self):
    #     return '{} {} {} {} {} {} {}'.format(self.project_id, self.title)


class ProjectSerializer(serializers.ModelSerializer):
    class Meta:
        model = Project
        fields = ["project_id", "title", "description", "task_number", "finished", "active"]
