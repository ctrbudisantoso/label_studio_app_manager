from django.contrib import admin
from django.urls import path
from . import views
from . import rest


urlpatterns = [
    path('', views.home, name='home'),
    path('admin/', views.admin_area, name='admin_area'),
    path('create/', views.add_projects, name='create_project'),
    path('upload_model/', views.upload_model, name='upload_model'),
    path('sync/', rest.ProjectSyncApiView.as_view(), name='project_sync_explorer'),
    path('sync/<int:id>/', rest.ProjectCrudApiView.as_view(), name='project_crud')
]