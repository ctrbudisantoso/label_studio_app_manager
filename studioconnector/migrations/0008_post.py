# Generated by Django 3.2.16 on 2023-05-19 10:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('studioconnector', '0007_project_active'),
    ]

    operations = [
        migrations.CreateModel(
            name='Post',
            fields=[
                ('id', models.BigAutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=255)),
                ('description', models.TextField()),
                ('image', models.FileField(blank=True, upload_to='')),
            ],
        ),
    ]
