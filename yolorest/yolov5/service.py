import os
import torch
from .yolov5.detect import run


from tempfile import mkstemp
from shutil import move, copymode
from os import fdopen, remove

def make_labels(source_path,project_path):
    result = run(weights='yolov5_200.pt', source=source_path, device='cpu'
                 , project=project_path, classes=[1, 2, 3, 4, 5,6, 7])
    print(result)
    return result


def replace_in_file(file_path, pattern, subst):
    #Create temp file
    fh, abs_path = mkstemp()
    with fdopen(fh,'w') as new_file:
        with open(file_path) as old_file:
            for line in old_file:
                new_file.write(line.replace(pattern, subst))
    #Copy the file permissions from the old file to the new file
    copymode(file_path, abs_path)
    #Remove original file
    remove(file_path)
    #Move new file
    move(abs_path, file_path)