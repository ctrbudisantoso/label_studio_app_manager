import os.path

from django.shortcuts import render

# Create your views here.
from django.shortcuts import render
from django.http import HttpResponse
from django.template import loader
from .models import Attribute, Project, ProjectSerializer, YoloModel
from .forms import SyncProjectForm, UploadYoloModelForm
import requests


def home(request):

    url = Attribute.get_label_studio_url()
    token = Attribute.get_label_studio_token()

    projects = Project.objects.order_by('project_id').all()
    serializer = ProjectSerializer(projects, many=True)

    data = serializer.data
    template = loader.get_template('studio_connector.html')
    context = {
        'data': data,
    }

    return HttpResponse(template.render(context, request))


def admin_area(request):
    if request.method == "POST":
        form = SyncProjectForm(request.POST)
        if form.is_valid():
            pass
    else:

        url = Attribute.get_label_studio_url()
        token = Attribute.get_label_studio_token()
        from_server = []
        complete_url = url + 'api/projects?ordering=id&include=id,description,title,task_number'

        r = requests.get(complete_url, headers={"Authorization": "Token " + token})
        results = r.json()['results']
        next_page = r.json()['next']

        for result in results:
            proj = {
                'project_id': result['id'],
                'title': result['title'],
                'description': result['description'],
                'task_number': result['task_number']

            }
            from_server.append(proj)

        while next_page is not None:
            r = requests.get(next_page, headers={"Authorization": "Token " + token})
            next_results = r.json()['results']
            next_page = r.json()['next']
            for result in next_results:
                proj = {
                    'project_id': result['id'],
                    'title': result['title'],
                    'description': result['description'],
                    'task_number': result['task_number']

                }
                from_server.append(proj)

        projects = Project.objects.order_by('project_id').all()
        serializer = ProjectSerializer(projects, many=True)
        projects = {'server': from_server, 'db': serializer.data}

        from_db = serializer.data
        sv_idx = 0
        table_content = []
        for db_p in from_db:
            proj_idx = db_p['project_id']
            sv_proj_idx = 0
            while sv_idx < len(from_server):
                sv_proj = from_server[sv_idx]
                temp_server_id = sv_proj['project_id']
                if temp_server_id > proj_idx:
                    break
                sv_proj_idx = temp_server_id
                col1_content = None
                if sv_proj_idx == proj_idx:
                    col1_content = {
                        'any_result':True,
                        'title':db_p['title'],
                        'project_id':db_p['project_id'],
                        'description':db_p['description']
                    }
                col2_content = {
                    'any_result':False,
                    'title':sv_proj['title'],
                    'project_id':sv_proj['project_id'],
                    'description':sv_proj['description']
                }

                table_content.append({
                    'db':col1_content,
                    'server':col2_content
                })

                sv_idx += 1
            if proj_idx > sv_proj_idx:
                col1_content = {
                    'any_result': True,
                    'title': db_p['title'],
                    'project_id': db_p['project_id'],
                    'description': db_p['description']
                }

                table_content.append({
                    'db':col1_content,
                    'server':None
                })
            last_proj_idx = proj_idx

        while sv_idx < len(from_server):
            sv_proj = from_server[sv_idx]
            temp_server_id = sv_proj['project_id']
            if temp_server_id > proj_idx:
                break
            sv_proj_idx = temp_server_id
            col1_content = None
            col2_content = {
                'any_result': False,
                'title': sv_proj['title'],
                'project_id': sv_proj['project_id'],
                'description': sv_proj['description']
            }

            table_content.append({
                'db': col1_content,
                'server': col2_content
            })
            sv_idx += 1
        print(len(from_db))
        attributes = Attribute.objects.all().values()
        # print(table_content)
        context = {
            'attributes': attributes,
            'url': url,
            'token': token,
            'projects': projects,
            'table_content':table_content
        }

        template = loader.get_template('admin_area.html')
        return HttpResponse(template.render(context, request))


def add_projects(request):
    attributes = Attribute.objects.all().values()
    model_useds = YoloModel.objects.all().values()
    context = {
        'attributes': attributes,
        'model_useds':model_useds
    }

    template = loader.get_template('add_project.html')
    return HttpResponse(template.render(context, request))


def upload_model(request):
    if request.method == 'POST':
        model_form = UploadYoloModelForm(request.POST,request.FILES)
        message = "Gagal"
        if model_form.is_valid():
            try:

                print('filename')
                model_file = request.FILES['model_file']
                filename = str(model_file)
                yolo_model = YoloModel()
                yolo_model.model_path = filename
                yolo_model.model_name = request.POST['model_name']
                print(filename)
                ws_path = os.getcwd()
                yolo_path = os.path.join(ws_path,'yolorest')
                yolo_path = os.path.join(yolo_path,'yolov5')
                filename = os.path.join(yolo_path,filename)
                with open(filename, "wb+") as destination:
                    for chunk in model_file.chunks():
                        destination.write(chunk)
                yolo_model.save()
                message = "Sukses"
            except:
                message = "Gagal"
            
        return render(request,"model_form_submited.html",{'message':message})
    else:
        model_form = UploadYoloModelForm()
        return render(request,"model_form.html",{'form':model_form})
